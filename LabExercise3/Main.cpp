// Lab Exercise 3
// Tyler Fink

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void Print(string text, ostream &out)
{
	out << text;
}

int main()
{
	string filepath = "C:\\temp\\test.txt";
	int questionNum = 10;
	string *question = new string[questionNum] {
		"Name", "School Subject", "Number", "Verb (plus ED)", "Verb", "Verb (plus ING)", "Same Name", "Adjective", "Letter of the Alphabet", "Plural Occupation" 
	};


	string story[11] = { "Tonight I'm going on a date with ", " from my ", " class. He has asked git me out over ", " times and I always ", " him away. This time, I thought I'd ", " for it. I mean, why not? Who is it ", "? ", "is one of those ", "kind of guys who gets straight ", "'s in every subject. He's kind of a ", "pet. It was Friday night when I heard a knock at the door.\n" };

	for (int i = 0; i < questionNum; i++)
	{
		cout << question[i] << ": \n" ;
		getline(cin, question[i]);
	}

	for (int i = 0; i < questionNum; i++)
	{
		Print(story[i], cout);
		Print(question[i], cout);
		if (i == 9)Print(story[10], cout);
	}


	ofstream fout(filepath);
	cout << "Do you want to save your story to a text file? (y/n): ";
	char ans;
	cin >> ans;


	if (ans == 'y' || ans == 'Y') {
		for (int i = 0; i < questionNum; i++)
		{
			Print(story[i], fout);
			Print(question[i], fout);
			if (i == 9)Print(story[10], fout);
			//Print("\n", cout);
		}
	}

	_getch();
	return 0;
}